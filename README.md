Bash script and systemd service to update Hurricane Electric's free dynamic
dns service. It uses curl.

```bash
git clone https://gitlab.com/rascul/he-ddns
cd he-ddns
sudo install -o root -g root -m 0755 he-ddns /usr/bin/
sudo install -o root -g root -m 0600 he-ddnsrc /etc/
sudoedit /etc/he-ddnsrc
sudo install -o root -g root -m 0644 he-ddns.{service,timer} /etc/systemd/system/
sudo systemctl daemon-reload
sudo systemctl enable --now he-ddns.timer
```
